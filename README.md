# MODELADO-DISEÑO-SOFTWARE

## Libros de referencia

[Aprendiendo UML](https://drive.google.com/file/d/1ngfm-1SunxEHhqXJQ-sR4T7KKjYbqzXH/view)

## Presentaciones UML

[00. Porqué fracasan Sistemas](00.Presentaciones_UML/00_Porqué_fracasan_Sistemas.ppt)

[01. Terminologia Basica](01_TerminologiaBasica.ppt)

[02. Procesos](02_Procesos.ppt)

[03. Mapas Mentales](03_MapasMentales.ppt)

[04. Idef](04_Idef.ppt)

[05. Introducción UML](05_IntroducciónUML.ppt)

[06. Como Usar UML](06_ComoUsarUML.ppt)

[07. Casos de Uso](07_CasosdeUso.ppt)

[08. Diagramas Interaccion](08_DiagramasInteraccion.ppt)

[09. Diagramas Clases Objetos](09_DiagramasClasesObjetos.ppt)

[10. Estados y Activades UML](10_EstadosyActivadesUML.ppt)

[11. Componentes Despliegue](11_ComponentesDespliegue.ppt)

[12. Patrones UML](12_PatronesUML.ppt)

[13. Rational Rose](13_RationalRose.ppt)

[14. Poseidon](14_Poseidon.ppt)

[15. Caso Practico UML](15_CasoPracticoUML.ppt)

[16. Idea Final](16_IdeaFinal.ppt)

[17. Metodo Analisis Sistemas](MetodoAnalisisSistemas.ppt)
